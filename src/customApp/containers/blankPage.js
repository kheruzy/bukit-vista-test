import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper.js";
import LayoutContent from "../../components/utility/layoutContent";
import { Col, Row, Icon } from 'antd';
import Input from '../../components/uielements/input';
import Box from '../../components/utility/box';
import LayoutWrapper from '../../components/utility/layoutWrapper.js';
import ContentHolder from '../../components/utility/contentHolder';
import IntlMessages from '../../components/utility/intlMessages';
// import Form from '../../components/uielements/form';
import { Form } from 'antd';
import Button from '../../components/uielements/button';
import { TimePicker } from 'antd';
import { APIConfig } from '../APIConfig'
import moment from 'moment';
const FormItem = Form.Item;

class GuestPortal extends Component {
  state = {
    bookingCode: "",
    booking: null,
    arrivalTime: null
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({bookingCode: values.bookingCode})
        this.loadData(values.bookingCode)
      }
    });
  };

  async loadData(bookingCode) {
    try {
      const {data} = await APIConfig.get("/booking/" + bookingCode)
      this.setState({booking: data})
      console.log(this.state.booking)
    }
    catch (error) {
      alert('Error in fetching booking. Please try again')
      console.log(error)
    }
  }

  async updateBooking(bookingCode) {
    try {
      const {data} = await APIConfig.put("/booking/" + bookingCode, this.state.booking)
      this.setState({booking: data.result})
      console.log(this.state.booking)
    }
    catch (error) {
      alert('Error in updating booking. Please try again')
      console.log(error)
    }
  }

  onTimeChange = (time, timestring) => {
    this.setState({arrivalTime: timestring})
  }

  handleTimeSubmit = e => {
    e.preventDefault()
    console.log("Hello")
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const newBooking = {...this.state.booking, arrivalTime: this.state.arrivalTime}
        this.setState({booking: newBooking})
        this.updateBooking(this.state.bookingCode)
      }
    });
  }

  render() {
    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap'
    };
    const colStyle = {
      marginBottom: '16px'
    };
    const { getFieldDecorator } = this.props.form;
    const momentObj = moment('00:00:00', 'HH:mm:ss')
    return (
      <LayoutContentWrapper style={{ height: "100%" }}>
        <LayoutContent>
          <h1>Guest Portal</h1>
          <br/>
          <Row style={rowStyle} justify="start">
            <Col md={12} sm={12} xs={24} style={colStyle}>
              <Box
                title={"Your Booking Code"}
              >
                <Form onSubmit={this.handleSubmit}>
                  <Form.Item>
                    {getFieldDecorator('bookingCode', {
                      rules: [
                        { required: true, message: 'Please input your booking code' },
                        { pattern: /[A-Z0-9 ]/, message: 'Booking code cannot contain lowercase letter(s) or symbol' },
                      ]
                    })(
                      <Input
                        placeholder="Input Your Booking Code Here" 
                        onChange={this.handleChange}
                      />,
                    )}
                  </Form.Item>
                  <FormItem colon={false}>
                    <Button type="primary" htmlType="submit">
                      Submit
                    </Button>
                  </FormItem>
                </Form>
              </Box>
            </Col>
          </Row>
          {
            this.state.booking == null?
            null :
            <Box>
              <h2>Your Booking</h2>
              <br/>
              <img src={this.state.booking.imageLink} style={{width: "50%"}}></img>
              <br/><br/>
              <p><b>Property Name</b></p>
              <p>{this.state.booking.propertyName}</p>
              <br/>
              <p><b>Check In</b></p>
              <p>{this.state.booking.checkIn}</p>
              <br/>
              <p><b>Check Out</b></p>
              <p>{this.state.booking.checkOut}</p>
              <br/>
              <p><b>Arrival Time</b></p>
              {this.state.booking.arrivalTime != ""? 
              <p>{this.state.booking.arrivalTime}</p> :
              <Form onSubmit = {this.handleTimeSubmit}>
                <p>Please Set Your Arrival Time</p>
                <TimePicker onChange={this.onTimeChange} defaultValue={momentObj}/>
                <FormItem colon={false}>
                  <Button type="primary" htmlType="submit">
                    Set Arrival Time
                  </Button>
                </FormItem>
              </Form>
              }   
            </Box>
          }
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}

const GuestPortalForm = Form.create({ name: 'guestPortalForm' })(GuestPortal);

export default GuestPortalForm