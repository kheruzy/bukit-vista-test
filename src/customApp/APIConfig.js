import axios from 'axios'

// General API Configuration for mock server
export const APIConfig = axios.create(
    {
        baseURL: 'https://8c120ca2-4198-449c-9840-f7c319b142a4.mock.pstmn.io',
    }
)